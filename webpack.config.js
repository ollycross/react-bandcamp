var path = require('path');
module.exports = {
    entry: './src/BandcampPlayer.jsx',
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'BandcampPlayer.js',
        libraryTarget: 'commonjs2'
    },
    module: {
        rules: [{
            test: /\.jsx$/,
            include: path.resolve(__dirname, 'src'),
            exclude: /(node_modules|bower_components|build)/,
            use: {
                loader: 'babel-loader',
            }
        }]
    },
    externals: {
        'react': 'commonjs react' // this line is just to use the React dependency of our parent-testing-project instead of using our own React.
    }
};