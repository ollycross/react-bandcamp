# React Bandcamp Player #

This component provides a simple way to add an embedded Bandcamp player to your project.

## Usage

    import BandcampPlayer from 'react-bandcamp'

    ...

    render() {
    	<BandcampPlayer album="xxxxx" />
    }


## Properties


- `album (required)` The ID of the album to embed.  
- `title` The title attribute of the `<iframe>`. Must be unique in DOM. Default `Bandcamp player`
- `size` Either `small` or `large`. Default: `large`.
- `bgcol` The hexcode of the background colour. Default:  `ffffff`.
- `linkcol` The hexcode of the link text colour. Default: `0687f5`,
- `merch` ID of merch to link to.  Ignored if `size` is not `large`. Default: `null`
- `tracklist` Whether to show tracklist under the player. Default: `true`
- `artwork` Size of artwork to show. `big` or `small`.  Default `big`.
- `width` CSS width for player iframe.  Is passed directly to `style` attribute so can be pixel size, percentage, `auto` etc. Default: `auto`
- `height` CSS height for player iframe.  Is passed directly to `style` attribute so can be pixel size, percentage, `auto` etc. Default: `auto`


IDs for albums / merch can be found by going to album on Bandcamp (e.g. 
<https://invinciblepigs.bandcamp.com/album/the-garden-folke>) and clicking the `share / embed` link under the album art, and examining the embed code.
